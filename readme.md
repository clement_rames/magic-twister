# Fab Challenge IV: Magic Twister ([Jana](https://jana_tothillcalvo.gitlab.io/website/), [Roger](https://roger_guilemany.gitlab.io/mdef-website/), [Clément](https://clement_rames.gitlab.io/mdef-website/) + [Pietro](https://pietro_rustici.gitlab.io/mdef-website/))

## Concept & purpose

For the last intervention, we wanted to find the best way to convey the results from the experiment in the line of research of biographies. The project has lead to a set of objects that the participants have developed as well as a reflection attached to each one of those. We want to share this reflections in a visual way, to embrace the different perspectives and approaches taken and open a debate around the topic. The final outcome will be a website with an online exhibition, showing the objects and its reflections. Also will work as a base for building up on top and implement further developments of the project. In order to show the objects we wanted to explore 3D scanning them to create a virtual universe where you can move, explore them and better grasp the idea behind our whole research on biographies of things.

## Workflow
### 3D Scanning Objects with Trnio

After doing some research, and acknowledging our limitations in terms of hardware and budget, we happily found [TRNIO](https://www.trnio.com). It is a mobile app (with a cost of around 5€) that uses photogrammetry to create 3D models. I requires a little of set up, lighting, space to move around... but the results ara satifying. We where a little bit concerned on how it would work with small objects such as ours, but it has been very good at its job. You can export .obj directly from the app to the computer (via Airdrop).

<img src="readme-content/scan01.gif" width="30%">
<img src="readme-content/scan02.gif" width="30%">

### Preparing the mesh with Meshmixer

From the 3D scan the camera would capture some unnecessary elements from the scene, such as background elements, the base where we placed the objects..., as well as missing some spots in the model. We have been using [MESHMIXER](https://www.meshmixer.com) to edit and repare the OBJ. It is a good option, very simple to use and free, even that it is developed by Autodesk.

<img src="readme-content/meshmixer.gif" width="60%">

### Assigning object origin with Blender

After editing on Meshmixer, it was important to have the origin of the objects in a good position, as we were intending to animate them. If the origin is not in place, we are incapable of properly defining the movement of the object. Although the workflow would had been simplified if we had done that in Meshmixer directly, we foud it way easier to edit the orgin on [BLENDER](https://www.blender.org). It is a very simple operation.

<img src="readme-content/blender.gif" width="60%">

### Animating the objects with Three.js
We wanted to create a virtual world in which to exhibit the objects from the nomadic box.
After exploring several options for 3D rendering in the browser, we settled on Three.js, a JavaScript library running WebGL. We create a scene, load each 3D scanned object into it, and animate them. We also enable the viewer to interact with the objects by clicking on them, which reveals their unique story.

## Coding logic
### HTML code
The static portion of our website is written in HTML. It simply consists in a  title and text boxes which are displayed upon click (using the CSS box class). Their position needs to be set as absolute so that they are overlaid on top of the JavaScript animation.
### CSS code
The CSS code is also very simple: it defines the formatting of  our page in terms of font, background color, etc. The  box class defines the attributes of pop-up boxes displaying the stories of objects.
### JavaScript code
The 3D animations are powered by JavaScript library Three.js. It requires the following modules: three.module.js, DDSLoader.js, OBJLoader.js, MTLLoader.js and OrbitControls.js. We instantiate six global variables at the start which will interact with our different functions: camera, scene, renderer (to set up the scene); controls, raycaster and mouse (to interact with the scene).

### Three.js functions
Our script is composed of three funtions: init(), loadModels() and animate().
The init() function creates the scene, camera and renderer as well as ambient light and a point light using the Three.js default library. The controls (zoom, pan, etc.) are leveraging the OrbitControls Three.js library.
The loadModels() function is called to load each object into the scene. It leverages the DDSLoader, OBJLoader and MTLLoader Three.js libraries. It first loads the object's mesh geometry, then applies the object's texture onto it. Finally, the position, size, and texture properties are defined.
The animate() function is the render loop running at 60 frames per second. It animates each object by rotating them about its x and y axis. It checks for mouse click events traverses the scene using a raytracer object. If any object is found in the raytracer's path, the corresponding text box pops up. If the user clicks outside of the objects, the text box is closed.

## Dissemination plan
Three.js is available under an MIT license, which is a permissive free software license.
We have already made our code available for a classmate to use in his project. We will strive to keep improving our documentation so as to make this contribution as accessible and helpful as possible.
A potential business model to generate income from this challenge would be to perform the whole workflow (3D scan, post-process models and animate in the browser) as a service. For example, food delivery apps could feature 3D models of their dishes.

# [Final result](https://clement_rames.gitlab.io/magic-twister/)
